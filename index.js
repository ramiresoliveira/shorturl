var app = require('./config/config-express')();
const MONGOURI = (process.env.NODE_ENV == 'prod' ? process.env.MONGOURIPROD : process.env.MONGOURIDEV);
require('./config/database')(MONGOURI);
var port = process.env.PORT || 3000;
app.listen(port, function(){
    console.log('Rodando em '+ (process.env.NODE_ENV == 'prod' ? 'PRODUÇÃO' : 'DESENVOLVIMENTO') +'...');
});