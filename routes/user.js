module.exports = function(app) {
	const userController = require("../controller/userController");
	
	app.get('/users', userController.getAll);

	app.get('/users/findBy/:userid', userController.findBy);

	app.post('/users', userController.save);

	app.post('/login', userController.login);
}