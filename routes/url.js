module.exports = function(app) {
  
  const urlController = require("../controller/urlController");
  
  app.get('/urls', urlController.getAll);

  app.get('/l/:shortUrl', urlController.redirect);

  app.post('/urls', urlController.save);

  app.put('/urls', urlController.update);

  app.delete('/urls', urlController.delete);
}