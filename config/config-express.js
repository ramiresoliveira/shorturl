require('dotenv').config();
var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var jwt = require('jsonwebtoken');
var fs = require('fs');
var path = require('path');
const SECRET = process.env.SECRET;

function verifica(req, res, next){
  
  if (((req.url=='/login')&&(req.method=='POST')) || (req.url.includes("/l/"))) {
      next();    
  } else {
    var token = req.body.token || req.query.token || req.headers['authorization'];
    if (token) {
      jwt.verify(token, SECRET, function (err, decoded) {
        if (err) {
            return res.json({ success: false, message: 'Falha ao tentar autenticar o token!' });
        } else {
            //se tudo correr bem, salver a requisição para o uso em outras rotas
            req.decoded = decoded;
            next();
        }
      });
    } else {
        // se não tiver o token, retornar o erro 403
        return res.status(403).send({
            success: false,
            message: '403 - Forbidden'
        });
    }
  }
}

module.exports = function(){
    var app = express();
    var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'})
    app.use(bodyParser.urlencoded({extended:true}));
    app.use(bodyParser.json());
    app.use(morgan('combined', {stream: accessLogStream}))
    app.use(verifica);
    consign()
        .include('models')
        .include('routes')
        .into(app);
    return app;
}