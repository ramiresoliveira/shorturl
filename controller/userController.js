const User = require("../models/userModel");
const SECRET = process.env.SECRET;
var jwt = require('jsonwebtoken');

exports.getAll = (req, res) => {
  User.find({}).lean().exec(function (e, data) {
    res.status(200).json(data);
  });
}

exports.findBy = (req, res) => {
  let userid = req.params.userid;
  if(!userid)
    return [];
  var promise = find(userid);
  promise.then(function(data){
    res.status(200).json(data)
  });
}

exports.find = (userid) => {
  var promise = find(userid);
  return promise;
}

function find (userid) {
  var promise = User.findOne({userid: userid}).exec();
  return promise;
}

exports.save = (req, res) => {
  let data = new User({
    userid: req.body.userid
  });

  data.save()
    .then(() => {
      res.status(201).json(data);
    })
    .catch(err => {
      res.status(500).json(err);
    });
}

exports.login = (req, res) => {
  let user = {
    userid: req.body.userid
  };
  var token = jwt.sign({
    data: user
  }, process.env.SECRET, { expiresIn: '1h' });
   
  res.json({
    success: true,
    message: 'Token criado!!!',
    token: token
    });   
}