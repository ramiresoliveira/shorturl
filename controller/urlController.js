const Url = require("../models/urlModel");

exports.getAll = (req, res) => {
  Url.find({}).lean().exec(function (e, data) {
    res.status(200).json(data)
  });
}

exports.save = (req, res) => {
  const userController = require('./userController')
  var tiny = getTiny();
  var userid = req.body.userid; 
  var userPromise = userController.find(userid);
  userPromise.then(function(userData){
    
    if(!userData)
      res.status(403).json({message: 'Usuário não encontrado.'});

    var data = new Url({
      userid: userData._id,
      url: req.body.url,
      short: tiny
    });
    data.save()
      .then(() => {
        res.status(201).json(data);
      })
      .catch(err => {
        res.status(500).json(err);
      });
  }, function(err){
    res.status(500).json(err);
  });
  
}

function getTiny() {
  var carac = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  var max = carac.length-1;
  var tiny = '';
  for(i = 0; i < 8; i++){
    tiny += carac[Math.floor((Math.random() * max) + 1)];
  }
  return tiny;
}

exports.update = (req, res) => {
  var _id  = req.body.id;
  var data = {
    userid: req.body.userid,
    url: req.body.url,
    short: req.body.short
  };
  
  Url.findByIdAndUpdate(_id, data, {new: true}, (err, url) => {
    if (err) {
      res.status(500).json(err);
    }
    res.status(202).json(url);
  });
}

exports.delete = (req, res) => {
  var _id = req.body.id;
  Url.findByIdAndRemove(_id, (err, url) => {
    if (err) {
      res.status(500).json(err);
    }
    const response = {
      message: "Excluído",
      id: url._id
    }
    res.status(200).json(response);
  });
}

exports.redirect = (req, res) => {
  var shortUrl = req.params.shortUrl;
  
  Url.findOne({short: shortUrl}, (err, data) => {
    if (data)
      res.redirect(301, data.url);
  });
}