let mongoose = require('mongoose');

let schema = new mongoose.Schema({
    userid: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('users', schema);