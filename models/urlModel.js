let mongoose = require('mongoose');

let schema = new mongoose.Schema({
    userid: {
        type: mongoose.Schema.ObjectId,
        ref: 'users'
    },
    url: {
        type: String,
        require: true
    },
    short: {
        type: String,
        require: true
    }

});

module.exports = mongoose.model('urls', schema);